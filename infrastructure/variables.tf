variable "resource_group_name" {
    type = string
    description = "(optional) describe your variable"
}

variable "resource_group_location" {
    type = string
    description = "(optional) describe your variable"
}

variable "storage_account_name" {
    type = string
    description = "(optional) describe your variable"
}

variable "app_service_plan_name" {
    type = string
    description = "(optional) describe your variable"
}

variable "function_app_name" {
    type = string
    description = "(optional) describe your variable"
}