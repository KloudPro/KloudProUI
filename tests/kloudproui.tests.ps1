param (
    $UserName,

    $Token
)

BeforeAll -Scriptblock {
    Push-Location .\infrastructure
    terraform init -backend-config="username=$($UserName)" -backend-config="password=$($Token)"
    tflint --init
    $script:tfvalidate = (terraform validate -json | ConvertFrom-Json)
    $script:tflint = (tflint --format json | ConvertFrom-Json)
    $script:tfsec = ((tfsec --custom-check-dir ..\custom_checks\ --format json | ConvertFrom-Json).results).where({ $_.rule_provider -eq 'custom' })
}

Describe -Name "kloudproui" -Fixture {
    Context -Name "generic test cases" -Fixture {
        It -Name "1 should be 1" {
            $UserName | Should -BeExactly "Chendrayan_Venkatesan"
        }
    }
    Context -Name "terraform validate" -Tag "unit.terraform.validate" -Fixture {
        It -Name "terrafrom validate should be true" {
            $script:tfvalidate.valid | Should -Be $true
        }

        It -Name "terraform validate severity should not be error" {
            $tfvalidate.diagnostics.foreach({ $_.severity | Should -Not -Be 'error' })
        }
    }
    
    Context -Name "terraform lint" -Tag "unit.tflint" -Fixture {
        It -Name "terraform linting errors should be null" {
            $tflint.errors | Should -Be $null
        }

        It -Name "terraform linting serverity should not have error" {
            $tflint.issues.rule.ForEach({ $_.severity | Should -BeExactly 'warning' })
        } 
    }

    Context -Name "terraform static code analysis" -Fixture {
        It -Name "tfsec should not throw severity HIGH alerts" -Test {
            $tfsec.ForEach({ $_.severity }) | Should -Not -BeIn @('HIGH') -Because "its non-compliant"
        }
    }
}

AfterAll -Scriptblock {
    Pop-Location
}